// eslint-disable-next-line no-unused-vars
import React from 'react';
import ReactDOM from 'react-dom';
import {initialiseStore} from './redux/Store';
import {BrowserRouter} from 'react-router-dom';
import App from './containers/App';
import TrackMobileView from './containers/TrackMobileView';

initialiseStore();

const appRoot = document.getElementById('app');

ReactDOM.render(
    <BrowserRouter>
      <TrackMobileView>
        <App />
      </TrackMobileView>
    </BrowserRouter>,
    appRoot,
);
