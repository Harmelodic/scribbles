import React from 'react';
import styled from 'styled-components';
import {Markdown} from '@harmelodic/react-ui-lib';
import {Store} from '../redux/Store';
import Actions from '../redux/Actions';
import Middleware from '../redux/Middleware';

const StyledViewPost = styled.div`
  & > div > h1:before, 
  & > div > h2:before, 
  & > div > h3:before {
    content: "";
    display: block;
    padding-top: 70px;
    margin-top: -65px;
  }
`;


export default class Post extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedPost: Store.getState().selectedPost,
      markdownText: Store.getState().markdownText,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.selectedPost.datePosted === undefined &&
      this.state.selectedPost.datePosted) {
      Store.dispatch(Actions.setAppBarTitle(this.state.selectedPost.title));
      Store.dispatch(
          Middleware.fetchMarkdown(this.state.selectedPost.fileName),
      );
    }
  }

  componentDidMount() {
    this.unsubscribe = Store.subscribe(() => {
      this.setState({
        selectedPost: Store.getState().selectedPost,
        markdownText: Store.getState().markdownText,
      });
    });

    Store.dispatch(Middleware.fetchPost(parseInt(this.props.match.params.id)));
    window.scroll(0, 0);
  }

  componentWillUnmount() {
    Store.dispatch(Actions.clearSelectedPost());
    Store.dispatch(Actions.clearMarkdownText());
    this.unsubscribe();
  }

  render() {
    return (
      <StyledViewPost>
        <Markdown
          markdown={this.state.markdownText}
          aTagAttributes='target="_blank" rel="nofollow"'/>
      </StyledViewPost>
    );
  }
}
