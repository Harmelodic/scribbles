import React from 'react';
import {Store} from '../redux/Store';
import Actions from '../redux/Actions';
import Middleware from '../redux/Middleware';
import Post from '../components/Post';
import FilterByBox from '../components/FilterByBox';
import InputTextBox from '../components/InputTextBox';
import Button from '../components/Button';
import styled from 'styled-components';

const StyledFilters = styled.div`
    text-align: ${(props) => props.mobileView ? 'center' : 'left'};
    white-space: normal;
`;

export default class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mobileView: Store.getState().mobileView,
      posts: Store.getState().posts,
      filterBySearch: Store.getState().filterBySearch,
      filterByLabel: Store.getState().filterByLabel,
    };

    this.onFilterBySearch = this.onFilterBySearch.bind(this);
    this.onFilterByLabel = this.onFilterByLabel.bind(this);
  }

  onFilterBySearch(event) {
    Store.dispatch(Actions.setFilterBySearch(event.target.value));
  }

  onFilterByLabel(event) {
    Store.dispatch(Actions.setFilterByLabel(event.target.value));
  }

  onClearFilters() {
    Store.dispatch(Actions.clearFilters());
  }

  componentDidMount() {
    this.unsubscribe = Store.subscribe(() => {
      this.setState({
        mobileView: Store.getState().mobileView,
        posts: Store.getState().posts,
        filterBySearch: Store.getState().filterBySearch,
        filterByLabel: Store.getState().filterByLabel,
      });
    });

    Store.dispatch(Middleware.fetchPosts());
    Store.dispatch(Actions.setAppBarTitle('Scribbles'));
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    const dateFormatOptions = {
      year: 'numeric',
      month: 'short',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
    };

    return (
      <div>
        <StyledFilters mobileView={this.state.mobileView}>
          <FilterByBox
            onChange={this.onFilterByLabel}
            value={this.state.filterByLabel}
          >
            <option value="">All</option>
            {
              this.state.posts
                  .flatMap((post) => post.labels)
                  .filter((value, index, array) =>
                    array.indexOf(value) === index)
                  .sort()
                  .map((label) => {
                    return (
                      <option key={label} value={label}>{label}</option>
                    );
                  })
            }
          </FilterByBox>
          <InputTextBox
            placeholder="Filter..."
            onChange={this.onFilterBySearch}
            value={this.state.filterBySearch}
          />
          <Button
            mobileView={this.state.mobileView}
            visible={(this.state.filterBySearch || this.state.filterByLabel)}
            onClick={this.onClearFilters}
          >
            Clear filters
          </Button>
        </StyledFilters>
        {
          this.state.posts
              .sort((a, b) => b.datePosted - a.datePosted)
              .filter((post) => post.title
                  .toUpperCase()
                  .includes(this.state.filterBySearch.toUpperCase()))
              .filter((post) => {
                if (this.state.filterByLabel === '') {
                  return post;
                } else {
                  return post.labels
                      .indexOf(this.state.filterByLabel) !== -1 ? post : null;
                }
              })
              .map((post) => {
                return (
                  <Post
                    key={post.datePosted}
                    link={`/${post.route}/${post.datePosted}`}
                    title={post.title}
                    labels={post.labels}
                    datePosted={
                      new Date(post.datePosted)
                          .toLocaleString('en-GB', dateFormatOptions)
                    }
                    lastUpdated={
                      new Date(post.lastUpdated)
                          .toLocaleString('en-GB', dateFormatOptions)
                    } />
                );
              })
        }
      </div>
    );
  }
}
