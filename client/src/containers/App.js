import React from 'react';
import {Switch, Route} from 'react-router-dom';
import ListPosts from './ListPosts';
import AppBar from '../components/AppBar';
import styled from 'styled-components';
import ViewPost from './ViewPost';
import FilmsSeen from './lists/FilmsSeen';
import TvShowsSeen from './lists/TvShowsSeen';
import {Store} from '../redux/Store';

const StyledAppContent = styled.div`
    margin: 0 auto 50vh auto;
    width: 100vw;
    max-width: 1080px;
`;

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mobileView: Store.getState().mobileView,
      appBarTitle: Store.getState().appBarTitle,
    };
  }

  componentDidMount() {
    this.unsubscribe = Store.subscribe(() => {
      this.setState({
        mobileView: Store.getState().mobileView,
        appBarTitle: Store.getState().appBarTitle,
      });
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    return (
      <div>
        <AppBar
          title={this.state.appBarTitle}
          mobileView={this.state.mobileView} />
        <StyledAppContent>
          <Switch>
            <Route exact path="/" component={ListPosts} />
            <Route exact path="/post/:id" component={ViewPost} />
            <Route exact path="/list/1532228220000" component={FilmsSeen} />
            <Route exact path="/list/1532228640000" component={TvShowsSeen} />
          </Switch>
        </StyledAppContent>
      </div>
    );
  }
}
