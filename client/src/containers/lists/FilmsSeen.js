import React from 'react';
import {Store} from '../../redux/Store';
import Middleware from '../../redux/Middleware';
import MediaListEntry from '../../components/MediaListEntry';
import Actions from '../../redux/Actions';
import SortPicker from '../../containers/SortPicker';

export default class FilmsSeen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filmsSeen: Store.getState().filmsSeen,
      sort: 'favourite',
      mobileView: Store.getState().mobileView,
    };

    this.onChangeSort = this.onChangeSort.bind(this);
  }

  onChangeSort(sort) {
    this.setState({
      sort: sort,
    });
  }

  componentDidMount() {
    this.unsubscribe = Store.subscribe(() => {
      this.setState({
        filmsSeen: Store.getState().filmsSeen,
        mobileView: Store.getState().mobileView,
      });
    });

    Store.dispatch(Actions.setAppBarTitle('Films I\'ve Seen'));
    Store.dispatch(Middleware.fetchFilmsSeen());
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    const filmsSeenWithPosition = this.state.filmsSeen.map((film, index) => {
      film.position = index + 1;
      return film;
    });

    return (
      <div>
        <div />
        <SortPicker
          selectedChoice={this.state.sort}
          onChangeSort={this.onChangeSort}
        />
        {
          filmsSeenWithPosition
              .sort((filmA, filmB) => {
                switch (this.state.sort) {
                  case 'alphabetical':
                    return filmA.primary_title
                        .localeCompare(filmB.primary_title);
                  case 'chronological':
                    return filmA.start_year - filmB.start_year;
                  default:
                    // Favourite
                    return filmA.position - filmB.position;
                }
              })
              .map((film) => {
                return (
                  <MediaListEntry
                    mobileView={this.state.mobileView}
                    key={film.tconst}
                    details={film}
                    descriptionTexts={[
                      'Year: ' + film.start_year,

                      `Director${(film.directors.length > 1 ? 's' : '')}:${
                        film.directors
                            .map((director) => '\n\t' + director).join(',')
                      }`,

                      `Writer${(film.writers.length > 1 ? 's' : '')}:${
                        film.writers
                            .map((writer) => '\n\t' + writer).join(',')
                      }`,

                      `Genres:${
                        film.genres
                            .split(',')
                            .map((genre) => ' ' + genre).join(',')
                      }`,

                      'Running Time: ' + film.runtime_minutes + ' minutes',
                    ]}
                  />
                );
              })
        }
      </div>
    );
  }
}
