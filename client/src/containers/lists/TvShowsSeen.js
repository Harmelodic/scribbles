import React from 'react';
import {Store} from '../../redux/Store';
import Middleware from '../../redux/Middleware';
import MediaListEntry from '../../components/MediaListEntry';
import Actions from '../../redux/Actions';
import SortPicker from '../../containers/SortPicker';

export default class TvShowsSeen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tvShowsSeen: Store.getState().tvShowsSeen,
      sort: 'favourite',
      mobileView: Store.getState().mobileView,
    };

    this.onChangeSort = this.onChangeSort.bind(this);
  }

  onChangeSort(sort) {
    this.setState({
      sort: sort,
    });
  }

  componentDidMount() {
    this.unsubscribe = Store.subscribe(() => {
      this.setState({
        tvShowsSeen: Store.getState().tvShowsSeen,
        mobileView: Store.getState().mobileView,
      });
    });

    Store.dispatch(Actions.setAppBarTitle('TV Shows I\'ve Seen'));
    Store.dispatch(Middleware.fetchTvShowsSeen());
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    const tvShowsSeenWithPosition = this.state.tvShowsSeen
        .map((tvShow, index) => {
          tvShow.position = index + 1;
          return tvShow;
        });

    return (
      <div>
        <SortPicker
          selectedChoice={this.state.sort}
          onChangeSort={this.onChangeSort}
        />
        {
          tvShowsSeenWithPosition
              .sort((tvShowA, tvShowB) => {
                switch (this.state.sort) {
                  case 'alphabetical':
                    return tvShowA.primary_title
                        .localeCompare(tvShowB.primary_title);
                  case 'chronological':
                    return tvShowA.start_year - tvShowB.start_year;
                  default:
                    // Favourite
                    return tvShowA.position - tvShowB.position;
                }
              })
              .map((tvShow) => {
                return (
                  <MediaListEntry
                    mobileView={this.state.mobileView}
                    key={tvShow.tconst}
                    details={tvShow}
                    descriptionTexts={[
                      `Original Release:${tvShow.start_year}-
                        ${(tvShow.end_year ? tvShow.end_year : 'Present')}
                      `,

                      `Creator${(tvShow.creators.length > 1 ? 's' : '')}:${
                        tvShow.creators
                            .map((creator) => '\n\t' + creator)
                            .join(',')
                      }`,

                      `Genres:${
                        tvShow.genres
                            .split(',')
                            .map((genre) => ' ' + genre)
                            .join(',')
                      }`,
                    ]}
                  />
                );
              })
        }
      </div>
    );
  }
}
