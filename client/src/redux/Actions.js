// Action Types
export const SET_MOBILE_VIEW = 'SET_MOBILE_VIEW';
export const SET_APP_BAR_TITLE = 'SET_APP_BAR_TITLE';
export const SET_POSTS = 'SET_POSTS';
export const SET_FILTER_BY_SEARCH = 'SET_FILTER_BY_SEARCH';
export const SET_FILTER_BY_LABEL = 'SET_FILTER_BY_LABEL';
export const CLEAR_FILTERS = 'CLEAR_FILTERS';
export const SET_SELECTED_POST = 'SET_SELECTED_POST';
export const SET_MARKDOWN_TEXT = 'SET_MARKDOWN_TEXT';

export const SET_FILMS_SEEN = 'SET_FILMS_SEEN';
export const SET_TV_SHOWS_SEEN = 'SET_TV_SHOWS_SEEN';


// Action Creators
export default class Actions {
  static setMobileView(mobileView) {
    return {
      type: SET_MOBILE_VIEW,
      mobileView,
    };
  }

  static setAppBarTitle(title) {
    return {
      type: SET_APP_BAR_TITLE,
      title,
    };
  }

  static setPosts(posts) {
    return {
      type: SET_POSTS,
      posts,
    };
  }

  static setFilterBySearch(filterBySearch) {
    return {
      type: SET_FILTER_BY_SEARCH,
      filterBySearch,
    };
  }

  static setFilterByLabel(filterByLabel) {
    return {
      type: SET_FILTER_BY_LABEL,
      filterByLabel,
    };
  }

  static clearFilters() {
    return {
      type: CLEAR_FILTERS,
    };
  }

  static setSelectedPost(selectedPost) {
    return {
      type: SET_SELECTED_POST,
      selectedPost,
    };
  }

  static clearSelectedPost() {
    return {
      type: SET_SELECTED_POST,
      selectedPost: {},
    };
  }

  static setMarkdownText(markdownText) {
    return {
      type: SET_MARKDOWN_TEXT,
      markdownText,
    };
  }

  static clearMarkdownText() {
    return {
      type: SET_MARKDOWN_TEXT,
      markdownText: '',
    };
  }

  static setFilmsSeen(films) {
    return {
      type: SET_FILMS_SEEN,
      films: films,
    };
  }

  static setTvShowsSeen(tvShows) {
    return {
      type: SET_TV_SHOWS_SEEN,
      tvShows: tvShows,
    };
  }
}
