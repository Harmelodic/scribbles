import {
  SET_MOBILE_VIEW,
  SET_APP_BAR_TITLE,
  SET_POSTS,
  SET_FILTER_BY_SEARCH,
  SET_FILTER_BY_LABEL,
  CLEAR_FILTERS,
  SET_SELECTED_POST,
  SET_MARKDOWN_TEXT,
  SET_FILMS_SEEN,
  SET_TV_SHOWS_SEEN,
} from './Actions';

export const rootReducer = (state, action) => {
  return {
    mobileView: mobileViewReducer(state.mobileView, action),
    appBarTitle: appBarTitleReducer(state.appBarTitle, action),
    posts: postsReducer(state.posts, action),
    filterBySearch: filterBySearchReducer(state.filterBySearch, action),
    filterByLabel: filterByLabelReducer(state.filterByLabel, action),
    selectedPost: selectedPostReducer(state.selectedPost, action),
    markdownText: markdownTextReducer(state.markdownText, action),
    filmsSeen: filmsSeenReducer(state.filmsSeen, action),
    tvShowsSeen: tvShowsSeenReducer(state.tvShowsSeen, action),
  };
};

export const mobileViewReducer = (mobileViewState, action) => {
  let mobileView = mobileViewState;

  switch (action.type) {
    case SET_MOBILE_VIEW:
      mobileView = action.mobileView;
      break;
  }

  return mobileView;
};

export const appBarTitleReducer = (appBarTitleState, action) => {
  let appBarTitle = appBarTitleState;

  switch (action.type) {
    case SET_APP_BAR_TITLE:
      appBarTitle = action.title;
      break;
  }

  return appBarTitle;
};

export const postsReducer = (postsState, action) => {
  let posts = Object.assign([], postsState);

  switch (action.type) {
    case SET_POSTS:
      posts = action.posts;
      break;
  }

  return posts;
};

export const filterBySearchReducer = (filterBySearchState, action) => {
  let filterBySearch = filterBySearchState;

  switch (action.type) {
    case SET_FILTER_BY_SEARCH:
      filterBySearch = action.filterBySearch;
      break;
    case CLEAR_FILTERS:
      filterBySearch = '';
      break;
  }

  return filterBySearch;
};

export const filterByLabelReducer = (filterByLabelState, action) => {
  let filterByLabel = filterByLabelState;

  switch (action.type) {
    case SET_FILTER_BY_LABEL:
      filterByLabel = action.filterByLabel;
      break;
    case CLEAR_FILTERS:
      filterByLabel = '';
      break;
  }

  return filterByLabel;
};

export const selectedPostReducer = (selectedPostState, action) => {
  let selectedPost = Object.assign({}, selectedPostState);

  switch (action.type) {
    case SET_SELECTED_POST:
      selectedPost = action.selectedPost;
      break;
  }

  return selectedPost;
};

export const markdownTextReducer = (markdownTextState, action) => {
  let markdownText = markdownTextState;

  switch (action.type) {
    case SET_MARKDOWN_TEXT:
      markdownText = action.markdownText;
      break;
  }

  return markdownText;
};

export const filmsSeenReducer = (filmsSeenState, action) => {
  let filmsSeen = Object.assign([], filmsSeenState);

  switch (action.type) {
    case SET_FILMS_SEEN:
      filmsSeen = action.films;
      break;
  }

  return filmsSeen;
};

export const tvShowsSeenReducer = (tvShowsSeenState, action) => {
  let tvShowsSeen = Object.assign([], tvShowsSeenState);

  switch (action.type) {
    case SET_TV_SHOWS_SEEN:
      tvShowsSeen = action.tvShows;
      break;
  }

  return tvShowsSeen;
};

