import Actions from './Actions';

const API_SERVER = process.env.API_SERVER || '';

const httpCall = (method, url, body) => {
  const request = {};
  request.method = method.toUpperCase();

  request.headers = new Headers();
  request.headers.append('Content-Type', 'application/json');

  if (!(request.method === 'GET' || request.method === 'HEAD')) {
    // To work with the Fetch API, the body needs to be stringified first.
    request.body = JSON.stringify(body);
  }

  return fetch(url, request)
      .catch((error) => {
        console.log(
            `Error occurred in completing ${request.method} request to: ${url}
            \n${error}`,
        );
      })
      .then((response) => {
        if (response.ok) {
          return response;
        } else {
          throw response;
        }
      });
};

export default class Middleware {
  static fetchPosts() {
    return (dispatch) => {
      return httpCall('GET', `${API_SERVER}/posts`)
          .then((response) => response.json().then((data) => {
            dispatch(Actions.setPosts(data));
          }));
    };
  }

  static fetchPost(id) {
    return (dispatch) => {
      return httpCall('GET', `${API_SERVER}/posts/${id}`)
          .then((response) => response.json().then((data) => {
            dispatch(Actions.setSelectedPost(data));
          }));
    };
  }

  static fetchMarkdown(fileName) {
    return (dispatch) => {
      dispatch(Actions.setMarkdownText(''));
      return httpCall('GET', `/posts/${fileName}`)
          .then((response) => response.text().then((data) => {
            dispatch(Actions.setMarkdownText(data));
          }));
    };
  }

  static fetchFilmsSeen() {
    return (dispatch) => {
      return httpCall('GET', '/posts/filmsSeen.json')
          .then((response) => response.json().then((data) => {
            dispatch(Actions.setFilmsSeen(data));
          }));
    };
  }

  static fetchTvShowsSeen() {
    return (dispatch) => {
      return httpCall('GET', '/posts/tvShowsSeen.json')
          .then((response) => response.json().then((data) => {
            dispatch(Actions.setTvShowsSeen(data));
          }));
    };
  }
}
