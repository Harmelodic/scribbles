import React from 'react';
import styled from 'styled-components';

const appBarHeight = '65px';

const StyledAppBar = styled.header`
    position: sticky;
    top: 0px;
    width: 100%;
    height: ${appBarHeight};
    background-color: #2d2d2d;
    font-size: 0;
`;

const StyledAppBarContent = styled.div`
    margin: 0 auto;
    width: 100%;
    max-width: 1080px;
`;

const StyledLogo = styled.img`
    display: inline-block;
    width: ${appBarHeight};
    height: ${appBarHeight};
    transition: background-color 150ms;

    &:hover {
        background-color: #444444;
    }
`;

const StyledPageTitle = styled.div`
    display: inline-block;
    margin: 0 ${(props) => props.mobileView ? '5px' : '30px'};
    height: 100%;
    width: calc(
      100% - ${appBarHeight} - calc(
        ${(props) => props.mobileView ? '5px' : '30px'} * 2
      )
    );
    font-size: ${(props) => props.mobileView ? '20px' : '28px'};
    color: white;
    line-height: ${appBarHeight};
    vertical-align: top;
    overflow: auto;
`;

export default class AppBar extends React.Component {
  render() {
    return (
      <StyledAppBar>
        <StyledAppBarContent>
          <a href="/">
            <StyledLogo src="/images/logo.svg"/>
          </a>
          <StyledPageTitle
            className="heading"
            mobileView={this.props.mobileView}
          >
            {this.props.title}
          </StyledPageTitle>
        </StyledAppBarContent>
      </StyledAppBar>
    );
  }
}
