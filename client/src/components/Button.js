import styled from 'styled-components';

const Button = styled.button`
    display: ${(props) =>
      props.mobileView && !props.visible ? 'none' : 'inline-block'};
    height: 40px;
    margin: 20px;
    background: #fff;
    border: solid 1px #bbb;
    border-radius: 5px;
    padding: 0 15px;
    font-size: 18px;
    color: #888;
    transition: border 200ms, color 200ms;
    visibility: ${(props) => !props.visible ? 'hidden' : 'visible'};

    &:focus {
        outline: none;
    }

    &:active {
        color: #333;
        border: solid 1px #333;
    }
`;

export default Button;
