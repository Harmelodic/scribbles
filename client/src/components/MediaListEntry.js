import React from 'react';
import styled, {css} from 'styled-components';

const StyledMediaListEntry = styled.div`
    width: 100%
    border-bottom: solid 1px #bbb;
`;

const StyledHalf = styled.div`
    display: ${(props) => props.mobileView ? 'block' : 'inline-block'};
    width: ${(props) => props.mobileView ? '100%' : '50%'};
`;

const StyledHalfLeft = styled(StyledHalf)`
    text-align: ${(props) => props.mobileView ? 'center' : 'right'};
`;

const StyledHalfRight = styled(StyledHalf)`
    text-align: ${(props) => props.mobileView ? 'center' : 'left'};
    vertical-align: top;
`;

// DVD Covers are 5x7.2" (129x183mm)
const pictureSizeFactor = 50;
const StyledPicture = styled.img`
    margin: 20px 0;
    width: calc(5 * ${pictureSizeFactor}px);
    height: calc(7.2 * ${pictureSizeFactor}px);
    border: solid 1px #333;
`;

const StyledDetailsWrapper = styled.div`
    margin: 20px 0 20px 50px;
    ${(props) => props.mobileView && css`
        display: inline-block;
        margin: 20px auto;
        padding: 0 20px;
    `}
    height: 100%;
    white-space: pre-wrap;
    text-align: left;
`;

const StyledDetailsText = styled.div`
    padding: 5px 0;
    color: #999999;
    font-size: 18px;
    font-style: italic;
`;

const StyledPosition = styled(StyledDetailsText)`
    display: inline-block;
    font-weight: normal;
`;

const StyledTitle = styled.div`
    font-size: 20px;
    font-weight: bold;
    color: #000000;
`;

export default class MediaListEntry extends React.PureComponent {
  render() {
    return (
      <StyledMediaListEntry>
        <StyledHalfLeft mobileView={this.props.mobileView}>
          <StyledPicture
            mobileView={this.props.mobileView}
            src={`/posts/posters/${this.props.details.tconst}.jpg`}
          />
        </StyledHalfLeft>
        <StyledHalfRight mobileView={this.props.mobileView}>
          <StyledDetailsWrapper mobileView={this.props.mobileView}>
            <StyledTitle>
              <StyledPosition>
                {`\u0023${this.props.details.position}: `}
              </StyledPosition>
              {this.props.details.primary_title}
            </StyledTitle>
            {
              this.props.descriptionTexts.map((text, index) => {
                return (
                  <StyledDetailsText key={index}>{text}</StyledDetailsText>
                );
              })
            }
          </StyledDetailsWrapper>
        </StyledHalfRight>
      </StyledMediaListEntry>
    );
  }
}
