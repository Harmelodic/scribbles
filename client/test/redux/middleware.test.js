import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import * as types from '../../src/redux/Actions';
import Middleware from '../../src/redux/Middleware';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('middleware', () => {
    afterEach(() => {
        fetchMock.restore()
    });

    it('creates SET_POSTS when fetching posts has been done', () => {
        fetchMock.getOnce('/posts', {
            headers: { 'Content-Type': 'application/json' },
            body: [{
                title: "Films I've Seen"
            }]
        });

        const expectedActions = [{
            type: types.SET_POSTS,
            posts: [{
                title: "Films I've Seen"
            }]
        }];

        const store = mockStore({});

        return store.dispatch(Middleware.fetchPosts())
            .then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
    });

    it('creates SET_SELECTED_POST when fetching single post has been done', () => {
        fetchMock.getOnce('/posts/10', {
            headers: { 'Content-Type': 'application/json' },
            body: {
                title: "Films I've Seen"
            }
        });

        const expectedActions = [{
            type: types.SET_SELECTED_POST,
            selectedPost: {
                title: "Films I've Seen"
            }
        }];

        const store = mockStore({});

        return store.dispatch(Middleware.fetchPost(10))
            .then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
    });

    it('creates SET_MARKDOWN_TEXT when fetching markdown file has been done', () => {
        fetchMock.getOnce('/posts/MarkdownText.md', {
            headers: { 'Content-Type': 'text/markdown' },
            body: '# Markdown'
        });

        const expectedActions = [
            {
                type: types.SET_MARKDOWN_TEXT,
                markdownText: ''
            },
            {
                type: types.SET_MARKDOWN_TEXT,
                markdownText: '# Markdown'
            }
        ];

        const store = mockStore({});

        return store.dispatch(Middleware.fetchMarkdown('MarkdownText.md'))
            .then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
    });

    it('creates SET_FILMS_SEEN when fetching films list has been done', () => {
        fetchMock.getOnce('/posts/filmsSeen.json', {
            headers: { 'Content-Type': 'application/json' },
            body: [{
                tconst: 10000
            }]
        });

        const expectedActions = [{
            type: types.SET_FILMS_SEEN,
            films: [{
                tconst: 10000
            }]
        }];

        const store = mockStore({});

        return store.dispatch(Middleware.fetchFilmsSeen())
            .then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
    });

    it('creates SET_TV_SHOWS_SEEN when fetching tv shows list has been done', () => {
        fetchMock.getOnce('/posts/tvShowsSeen.json', {
            headers: { 'Content-Type': 'application/json' },
            body: [{
                tconst: 3333
            }]
        });

        const expectedActions = [{
            type: types.SET_TV_SHOWS_SEEN,
            tvShows: [{
                tconst: 3333
            }]
        }];

        const store = mockStore({});

        return store.dispatch(Middleware.fetchTvShowsSeen())
            .then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
    });
});
