import * as types from '../../src/redux/Actions';
import * as reducers from '../../src/redux/Reducers';

const initialState = {
    mobileView: false,
    appBarTitle: undefined,
    posts: [],
    filterBySearch: '',
    filterByLabel: '',
    selectedPost: {},
    markdownText: '',
    filmsSeen: [],
    tvShowsSeen: [],
  };  

describe('mobileView reducer', () => {
  it('should return the initial state', () => {
    expect(reducers.mobileViewReducer(initialState.mobileView, {})).toEqual(false)
  })

  it('should handle SET_MOBILE_VIEW', () => {
    expect(reducers.mobileViewReducer(initialState.mobileView, {
        type: types.SET_MOBILE_VIEW,
        mobileView: true
    })).toEqual(true)

    expect(reducers.mobileViewReducer(initialState.mobileView, {
        type: types.SET_MOBILE_VIEW,
        mobileView: false
    })).toEqual(false)
  })
})

describe('appBarTitle reducer', () => {
    it('should return the initial state', () => {
        expect(reducers.appBarTitleReducer(initialState.appBarTitle, {})).toEqual(undefined)
    })

    it('should handle SET_APP_BAR_TITLE', () => {
        expect(reducers.appBarTitleReducer(initialState.appBarTitle, {
            type: types.SET_APP_BAR_TITLE,
            title: 'Something'
        })).toEqual('Something')
    })
})

describe('posts reducer', () => {
    it('should return the initial state', () => {
        expect(reducers.postsReducer(initialState.posts, {})).toEqual([])
    })

    it('should handle SET_POSTS', () => {
        expect(reducers.postsReducer(initialState, {
            type: types.SET_POSTS,
            posts: ['Some Post']
        })).toEqual(['Some Post'])
    })
})

describe('filterBySearch reducer', () => {
    it('should return the initial state', () => {
        expect(reducers.filterBySearchReducer(initialState.filterBySearch, {})).toEqual('')
    })

    it('should handle SET_FILTER_BY_SEARCH', () => {
        expect(reducers.filterBySearchReducer(initialState.filterBySearch, {
            type: types.SET_FILTER_BY_SEARCH,
            filterBySearch: 'Search String'
        })).toEqual('Search String')
    })

    it('should handle CLEAR_FILTERS', () => {
        expect(reducers.filterBySearchReducer('Some Search String', {
            type: types.CLEAR_FILTERS,
        })).toEqual('')
    })
})

describe('filterByLabel reducer', () => {
    it('should return the initial state', () => {
        expect(reducers.filterByLabelReducer(initialState.filterByLabel, {})).toEqual('')
    })

    it('should handle SET_FILTER_BY_LABEL', () => {
        expect(reducers.filterByLabelReducer(initialState.filterByLabel, {
            type: types.SET_FILTER_BY_LABEL,
            filterByLabel: 'Filter String'
        })).toEqual('Filter String')
    })

    it('should handle CLEAR_FILTERS', () => {
        expect(reducers.filterByLabelReducer('Some Label String', {
            type: types.CLEAR_FILTERS,
        })).toEqual('')
    })
})

describe('selectedPost reducer', () => {
    it('should return the initial state', () => {
        expect(reducers.selectedPostReducer(initialState.selectedPost, {})).toEqual({})
    })

    it('should handle SET_SELECTED_POST', () => {
        expect(reducers.selectedPostReducer(initialState.selectedPost, {
            type: types.SET_SELECTED_POST,
            selectedPost: {
                title: 'Post Title'
            }
        })).toEqual({
            title: 'Post Title'
        })
    })
})

describe('markdownText reducer', () => {
    it('should return the initial state', () => {
        expect(reducers.markdownTextReducer(initialState.markdownText, {})).toEqual('')
    })

    it('should handle SET_MARKDOWN_TEXT', () => {
        expect(reducers.markdownTextReducer(initialState.markdownText, {
            type: types.SET_MARKDOWN_TEXT,
            markdownText: '# Some Markdown Text'
        })).toEqual('# Some Markdown Text')
    })
})

describe('filmsSeen reducer', () => {
    it('should return the initial state', () => {
        expect(reducers.filmsSeenReducer(initialState.filmsSeen, {})).toEqual([])
    })

    it('should handle SET_FILMS_SEEN', () => {
        expect(reducers.filmsSeenReducer(initialState.filmsSeen, {
            type: types.SET_FILMS_SEEN,
            films: [
                'The Matrix'
            ]
        })).toEqual([
            'The Matrix'
        ])
    })
})

describe('tvShowsSeen reducer', () => {
    it('should return the initial state', () => {
        expect(reducers.tvShowsSeenReducer(initialState.tvShowsSeen, {})).toEqual([])
    })

    it('should handle SET_TV_SHOWS_SEEN', () => {
        expect(reducers.tvShowsSeenReducer(initialState.tvShowsSeen, {
            type: types.SET_TV_SHOWS_SEEN,
            tvShows: [
                'Mr. Robot'
            ]
        })).toEqual([
            'Mr. Robot'
        ])
    })
})
