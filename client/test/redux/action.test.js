import * as types from '../../src/redux/Actions';
import Actions from '../../src/redux/Actions';

describe('actions', () => {
  it('should create an action to set Mobile View to true', () => {
    const mobileView = true;
    const expectedAction = {
      type: types.SET_MOBILE_VIEW,
      mobileView
    }
    expect(Actions.setMobileView(mobileView)).toEqual(expectedAction);
  });

  it('should create an action to set Mobile View to false', () => {
    const mobileView = false;
    const expectedAction = {
      type: types.SET_MOBILE_VIEW,
      mobileView
    }
    expect(Actions.setMobileView(mobileView)).toEqual(expectedAction);
  });

  it('should create an action to set App Bar Title', () => {
    const title = 'Some Random Text';
    const expectedAction = {
      type: types.SET_APP_BAR_TITLE,
      title
    }
    expect(Actions.setAppBarTitle(title)).toEqual(expectedAction);
  });

  it('should create an action to set Posts', () => {
    const posts = [];
    const expectedAction = {
      type: types.SET_POSTS,
      posts
    }
    expect(Actions.setPosts(posts)).toEqual(expectedAction);
  });

  it('should create an action to set Filter By Search', () => {
    const filterBySearch = 'Some Random Text';
    const expectedAction = {
      type: types.SET_FILTER_BY_SEARCH,
      filterBySearch
    }
    expect(Actions.setFilterBySearch(filterBySearch)).toEqual(expectedAction);
  });
  
  it('should create an action to set Filter By Label', () => {
    const filterByLabel = 'Some Random Text';
    const expectedAction = {
      type: types.SET_FILTER_BY_LABEL,
      filterByLabel
    }
    expect(Actions.setFilterByLabel(filterByLabel)).toEqual(expectedAction);
  });

  it('should create an action to clear filters', () => {
    const expectedAction = {
      type: types.CLEAR_FILTERS
    }
    expect(Actions.clearFilters()).toEqual(expectedAction);
  });
  
  it('should create an action to set the selected post', () => {
    const selectedPost = { title: 'Post Title' };
    const expectedAction = {
      type: types.SET_SELECTED_POST,
      selectedPost
    }
    expect(Actions.setSelectedPost(selectedPost)).toEqual(expectedAction);
  });

  it('should create an action to clear the selected post', () => {
    const expectedAction = {
      type: types.SET_SELECTED_POST,
      selectedPost: {}
    }
    expect(Actions.clearSelectedPost()).toEqual(expectedAction);
  });

  it('should create an action to set Markdown Text', () => {
    const markdownText = '# Some Random Text';
    const expectedAction = {
      type: types.SET_MARKDOWN_TEXT,
      markdownText
    }
    expect(Actions.setMarkdownText(markdownText)).toEqual(expectedAction);
  });

  it('should create an action to clear Markdown Text', () => {
    const expectedAction = {
      type: types.SET_MARKDOWN_TEXT,
      markdownText: ''
    }
    expect(Actions.clearMarkdownText()).toEqual(expectedAction);
  });
  
  it('should create an action to set Films Seen', () => {
    const films = [];
    const expectedAction = {
      type: types.SET_FILMS_SEEN,
      films
    }
    expect(Actions.setFilmsSeen(films)).toEqual(expectedAction);
  });

  it('should create an action to set TV Shows Seen', () => {
    const tvShows = [];
    const expectedAction = {
      type: types.SET_TV_SHOWS_SEEN,
      tvShows
    }
    expect(Actions.setTvShowsSeen(tvShows)).toEqual(expectedAction);
  });
});
