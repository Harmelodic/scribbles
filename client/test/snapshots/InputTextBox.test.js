import React from 'react';
import renderer from 'react-test-renderer';
import InputTextBox from '../../src/components/InputTextBox';
import 'jest-styled-components';

it('renders correctly', () => {
  const tree = renderer.create(<InputTextBox />).toJSON();
  expect(tree).toMatchSnapshot();
});