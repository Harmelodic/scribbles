import React from 'react';
import renderer from 'react-test-renderer';
import AppBar from '../../src/components/AppBar';
import 'jest-styled-components';

it('renders correctly', () => {
    const tree = renderer.create(<AppBar title='App Bar Title' />).toJSON();
    expect(tree).toMatchSnapshot();
});


it('renders correctly - mobile', () => {
    const tree = renderer.create(<AppBar title='Mobile App Bar Title' mobileView={true}/>).toJSON();
    expect(tree).toMatchSnapshot();
});