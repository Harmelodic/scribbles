import React from 'react';
import renderer from 'react-test-renderer';
import Post from '../../src/components/Post';
import 'jest-styled-components';

const post = {
    "title": "Films I've Seen",
    "route": "list",
    "datePosted": 1532228220000,
    "lastUpdated": 1577980784000,
    "fileName": "filmsSeen",
    "labels": [
        "List"
    ]
}

it('renders correctly', () => {
    const tree = renderer
        .create(
            <Post
                key='1532228220000'
                link={`/list/1532228220000`}
                title={post.title}
                labels={post.labels}
                datePosted="22 Jul 2018, 03:57"
                lastUpdated="02 Jan 2020, 15:59" />
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});