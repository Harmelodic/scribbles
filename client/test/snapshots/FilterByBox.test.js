import React from 'react';
import renderer from 'react-test-renderer';
import FilterByBox from '../../src/components/FilterByBox';
import 'jest-styled-components';

it('renders correctly', () => {
  const tree = renderer.create(<FilterByBox />).toJSON();
  expect(tree).toMatchSnapshot();
});