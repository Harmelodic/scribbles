import React from 'react';
import renderer from 'react-test-renderer';
import MediaListEntry from '../../src/components/MediaListEntry';
import 'jest-styled-components';

it('renders correctly', () => {
    const tree = renderer.create(
        <MediaListEntry 
            details={{
                tconst: 't000000',
                position: 1,
                primary_title: 'Media Title'
            }}
            descriptionTexts={[
            'Year: ',
            'Director',
            'Writer',
            'Genres:',
            'Running Time: minutes',
            ]} />
    ).toJSON();
    expect(tree).toMatchSnapshot();
});


it('renders correctly - mobile', () => {
    const tree = renderer.create(
        <MediaListEntry 
            details={{
                tconst: 't000000',
                position: 1,
                primary_title: 'Media Title'
            }}
            descriptionTexts={[
            'Year: ',
            'Director',
            'Writer',
            'Genres:',
            'Running Time: minutes',
            ]}
            mobileView={true} />
    ).toJSON();
    expect(tree).toMatchSnapshot();
});